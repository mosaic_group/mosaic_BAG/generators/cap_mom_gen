v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 160 -290 160 -270 {
lab=plus}
N 160 -210 160 -190 {
lab=xp}
N 160 -140 160 -120 {
lab=xn}
N 160 -60 160 -40 {
lab=minus}
C {devices/iopin.sym} 100 -240 2 0 {name=p2 lab=plus}
C {devices/iopin.sym} 100 -280 2 0 {name=p3 lab=VSS}
C {devices/lab_pin.sym} 160 -290 2 0 {name=l4 sig_type=std_logic lab=plus}
C {BAG_prim/res_metal_5/res_metal_5.sym} 160 -240 0 0 {name=XP
w=1u
l=2u
spiceprefix=X
}
C {devices/iopin.sym} 100 -220 2 0 {name=p1 lab=minus}
C {devices/lab_pin.sym} 160 -190 2 0 {name=l1 sig_type=std_logic lab=xp}
C {devices/lab_pin.sym} 160 -140 2 0 {name=l7 sig_type=std_logic lab=xn}
C {BAG_prim/res_metal_5/res_metal_5.sym} 160 -90 0 0 {name=XN
w=1u
l=2u
spiceprefix=X
}
C {devices/lab_pin.sym} 160 -40 2 0 {name=l8 sig_type=std_logic lab=minus}
