from typing import *
from abs_templates_ec.analog_mos.mos import DummyFillActive
from bag.layout.routing import TrackManager, TrackID
from bag.layout.template import TemplateBase
from bag.layout.util import BBox

from .params import cap_mom_layout_params, TrackInfo
from .legacy.SAL_havefun3 import SAL_havefun3, TechInfoConfig_SAL    # TODO: clean this up


class layout(TemplateBase):
    """
    A metal-only finger cap.
    ports are drawn on the layer above param.top_layer.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='cap_mom_layout_params parameter object',
        )

    def draw_layout(self):
        params: cap_mom_layout_params = self.params['params']

        res = self.grid.resolution
        io_layer = params.top_layer 
        w_tot = params.width + 2 * params.margin
        h_tot = params.height + 2 * params.margin
        if params.fill_config is None:
            w_blk, h_blk = self.grid.get_block_size(layer_id=io_layer, unit_mode=True,
                                                    half_blk_x=params.half_blk_x, half_blk_y=params.half_blk_y)
        else:
            w_blk, h_blk = self.grid.get_fill_size(top_layer=io_layer, fill_config=params.fill_config, unit_mode=True,
                                                   half_blk_x=params.half_blk_x, half_blk_y=params.half_blk_y)
        w_tot = -(-w_tot // w_blk) * w_blk
        h_tot = -(-h_tot // h_blk) * h_blk

        # set size
        bnd_box = BBox(left=0, bottom=0, right=w_tot, top=h_tot,
                       resolution=res, unit_mode=True)
        self.array_box = bnd_box
        self.set_size_from_bound_box(top_layer_id=io_layer, bbox=bnd_box)
        self.add_cell_boundary(box=bnd_box)

        # get input/output track location
        io_horiz = self.grid.get_direction(io_layer) == 'x'
        mid_coord = bnd_box.yc_unit if io_horiz else bnd_box.xc_unit
        io_tidx = self.grid.coord_to_nearest_track(layer_id=io_layer, coord=mid_coord,
                                                   half_track=True, mode=0, unit_mode=True)
        in_track_info = params.in_tid
        out_track_info = params.out_tid
        if params.in_tid is None:
            in_track_info = TrackInfo(index=io_tidx, width=3)
        if params.out_tid is None:
            out_track_info = TrackInfo(index=io_tidx, width=3)
        in_tid = TrackID(layer_id=io_layer, track_idx=in_track_info.index, width=in_track_info.width)
        out_tid = TrackID(layer_id=io_layer, track_idx=out_track_info.index, width=out_track_info.width)

        # setup capacitor options
        # get port width dictionary.  Make sure we can via up to params.top_layer + 1
        in_w = self.grid.get_track_width(layer_id=io_layer, width_ntr=in_tid.width, unit_mode=True)
        out_w = self.grid.get_track_width(layer_id=io_layer, width_ntr=out_tid.width, unit_mode=True)
        top_port_tr_w = self.grid.get_min_track_width(layer_id=params.top_layer, top_w=max(in_w, out_w),
                                                      unit_mode=True)
        top_port_tr_w = max(top_port_tr_w, params.port_tr_w)
        port_tr_w_dict = {lay: params.port_tr_w for lay in range(params.bot_layer, params.top_layer + 1)}
        port_tr_w_dict[params.top_layer] = top_port_tr_w

        options = params.options
        if options is None:
            options = {}
        options['port_widths'] = port_tr_w_dict

        # draw cap
        cap_xl = (bnd_box.width_unit - params.width) // 2
        cap_yb = (bnd_box.height_unit - params.height) // 2
        cap_box = BBox(left=cap_xl, bottom=cap_yb, right=cap_xl + params.width, top=cap_yb + params.height,
                       resolution=res, unit_mode=True)
        print('cap_box=', cap_box)
        num_layer = params.top_layer - params.bot_layer + 1

        cap_direction = self.grid.get_direction(io_layer)  # direction of cap is determined by IO layer direction

        # TODO: clean up legacy helper code
        tech_info = self.grid.tech_info
        TechInfoConfig_SAL.init_tech_info(tech_info)
        cap_ports = SAL_havefun3.add_mom_cap_v4(
            self,
            cap_box=cap_box,
            bot_layer=params.bot_layer,
            num_layer=num_layer,
            cap_direction=cap_direction,
            **options
        )
        port_wire_array = cap_ports  # return the wire array of the capacitor ports situated in the center
        cout, cin = port_wire_array  # Getting wire information about capacitor's ports(i.e [MINUS, PLUS] terminals)
        in_min_len = self.grid.get_min_length(layer_id=io_layer, width_ntr=in_tid.width, unit_mode=True)

        res_upper = cin.lower_unit
        res_lower = res_upper - in_min_len
        in_lower = min(0, res_lower - in_min_len)
        if io_layer == params.top_layer + 1:
            self.connect_to_tracks(wire_arr_list=cin, track_id=in_tid, track_lower=in_lower, unit_mode=True)
        self.add_res_metal_warr(layer_id=io_layer, track_idx=in_track_info.index, lower=res_lower, upper=res_upper,
                                width=in_tid.width, unit_mode=True)
        in_warr = self.add_wires(layer_id=io_layer, track_idx=in_track_info.index, lower=in_lower, upper=res_lower,
                                 width=in_tid.width, unit_mode=True)

        out_min_len = self.grid.get_min_length(layer_id=io_layer, width_ntr=out_tid.width, unit_mode=True)

        res_lower = cout.upper_unit
        res_upper = res_lower + out_min_len
        if self.grid.get_direction(io_layer) == 'x':
            out_upper = max(w_tot, res_upper + out_min_len)
        else:
            out_upper = max(h_tot, res_upper + out_min_len)

        if io_layer == params.top_layer + 1:
            self.connect_to_tracks(wire_arr_list=cout, track_id=out_tid, track_upper=out_upper, unit_mode=True)

        self.add_res_metal_warr(layer_id=io_layer, track_idx=out_track_info.index, lower=res_lower, upper=res_upper,
                                width=out_tid.width, unit_mode=True)
        out_warr = self.add_wires(layer_id=io_layer, track_idx=out_track_info.index, lower=res_upper, upper=out_upper,
                                  width=out_tid.width, unit_mode=True)

        self.add_pin(net_name='plus', wire_arr_list=in_warr, show=params.show_pins)
        self.add_pin(net_name='minus', wire_arr_list=out_warr, show=params.show_pins)

        if params.fill_dummy:
            for lay in range(1, io_layer + 1):
                self.do_max_space_fill(layer_id=lay, bound_box=bnd_box, fill_pitch=params.fill_pitch)
            dum_params = dict(
                mos_type=params.mos_type,
                threshold=params.threshold,
                width=w_tot,
                height=h_tot
            )
            master_dum = self.new_template(temp_cls=DummyFillActive, params=dum_params)
            self.add_instance(master=master_dum, unit_mode=True)

        lay_unit = self.grid.layout_unit
        self._sch_params = dict(
            res_in_info=(io_layer,
                         in_w * res * lay_unit,
                         in_min_len * res * lay_unit,
                         "metal_5"),
            res_out_info=(io_layer,
                          out_w * res * lay_unit,
                          out_min_len * res * lay_unit,
                          "metal_5"),
            sub_name=params.sub_name,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class cap_mom(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
