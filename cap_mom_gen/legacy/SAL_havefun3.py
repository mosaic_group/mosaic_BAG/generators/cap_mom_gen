# -*- coding: utf-8 -*-


"""This module defines layout template classes.
"""

from typing import Set, Callable, TYPE_CHECKING, Union, List, Tuple, Optional, Dict, Any
import pkg_resources
import os

from itertools import chain

import yaml

import BAG2_technology_definition
import bag
import abc
import numpy as np
import bag.layout.routing.grid
import bag.io
import bag.layout.tech
from bag.layout.util import tuple2_to_int, BBox, BBoxArray
from bag.io import get_encoding
from bag.layout.routing import TrackID, WireArray
from bag.layout.objects import Via, ViaInfo, Arrayable
from bag.layout.template import TemplateDB, TemplateBase



try:
    import cybagoa
except ImportError:
    cybagoa = None
try:
    # noinspection PyPackageRequirements
    import gdspy
except ImportError:
    gdspy = None





ldim = Union[float, int]
loc_type = Tuple[ldim, ldim]


#
# _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('data', 'tech_params.yaml'))
#
# with open(_yaml_file, 'r') as f:
#     config = yaml.load(f, Loader=yaml.FullLoader)






class SAL_havefun3(TemplateBase):



    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
         # type: (TemplateDB, str, Dict[str, Any], Set[str], **kwargs) -> None
         TemplateBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
         self._sch_params = None


    def add_mom_cap_v4(self,  # type: TemplateBase
                    cap_box,  # type: BBox
                    bot_layer,  # type: int
                    num_layer,  # type: int
                    cap_direction,
                    port_widths=1,  # type: Union[int, List[int], Dict[int, int]]
                    port_parity=None,
                    # type: Optional[Union[Tuple[int, int], Dict[int, Tuple[int, int]]]]
                    array=False,  # type: bool
                    **kwargs
                    ):
        # type: (...) -> Any



        """Draw mom cap in the defined bounding box."""
        track_width = self.grid.get_track_width(bot_layer, port_widths[bot_layer], unit_mode=False)
        track_pitch = self.grid.get_track_pitch(bot_layer, unit_mode=False)
        track_space = track_pitch - track_width
        track_init = 0.5 * track_space + 0.5 * track_width

        bot_direction = self.grid.get_direction(bot_layer)
        # if cap_direction is None:
        #     cap_direction = bot_direction
        is_horizontal = (cap_direction == 'x')

        return_rect = kwargs.get('return_cap_wires', False)
        cap_type = kwargs.get('cap_type', 'standard')

        if num_layer <= 1:
            raise ValueError('Must have at least 2 layers for MOM cap.')

        res = self.grid.resolution
        tech_info = self.grid.tech_info

        mom_cap_dict = tech_info.tech_params['layout']['mom_cap'][cap_type]
        # mom_cap_dict = tech_info.tech_params['layout']['mom_cap'][cap_type]
        cap_margins = mom_cap_dict['margins']
        cap_info = mom_cap_dict['width_space']
        num_ports_on_edge = mom_cap_dict.get('num_ports_on_edge', {})
        port_widths_default = mom_cap_dict.get('port_widths_default', {})
        port_sp_min = mom_cap_dict.get('port_sp_min', {})

        top_layer = bot_layer + num_layer - 1

        if isinstance(port_widths, int):
            port_widths = {lay: port_widths for lay in range(bot_layer, top_layer + 1)}
        elif isinstance(port_widths, list) or isinstance(port_widths, tuple):
            if len(port_widths) != num_layer:
                raise ValueError('port_widths length != %d' % num_layer)
            port_widths = dict(zip(range(bot_layer, top_layer + 1), port_widths))
        else:
            port_widths = {lay: port_widths.get(lay, port_widths_default.get(lay, 1))
                           for lay in range(bot_layer, top_layer + 1)}

        if port_parity is None:
            port_parity = {lay: (0, 1) for lay in range(bot_layer, top_layer + 1)}
        elif isinstance(port_parity, tuple) or isinstance(port_parity, list):
            if len(port_parity) != 2:
                raise ValueError('port parity should be a tuple/list of 2 elements.')
            port_parity = {lay: port_parity for lay in range(bot_layer, top_layer + 1)}
        else:
            port_parity = {lay: port_parity.get(lay, (0, 1)) for lay in
                           range(bot_layer, top_layer + 1)}

        via_ext_dict = {lay: 0 for lay in range(bot_layer, top_layer + 1)}  # type: Dict[int, int]
        # get via extensions on each layer
        # a = range(bot_layer, top_layer)

        #------------------------------------------------------drc info//
        via_name = self.grid.tech_info.get_via_name(top_layer-1)
        via_bot_met_name = self.grid.tech_info.get_layer_name(top_layer - 1)
        via_bot_met_name = via_bot_met_name[0] if isinstance(via_bot_met_name, Tuple) else via_bot_met_name
        via_bot_met_type = self.grid.tech_info.get_layer_type(via_bot_met_name)
        drc_info_one = TechInfoConfig_SAL.get_via_drc_info(
            TechInfoConfig_SAL, via_name, 'square', via_bot_met_type, 22, True
        )
        sp, sp2_list, sp3_list, sp6_list, dim, encb, arr_encb, arr_testb = drc_info_one
        w_single_via = (dim[0] + encb[0][0] + encb[0][0]) * 1
        w_two_via = (sp[0] + dim[0] + dim[0] + encb[0][0] + encb[0][0]) * 1
        #------------------------------------------------------drc info//


        lower_x = None
        upper_x = None
        lower_y = None
        upper_y = None
        for vbot_layer in range(bot_layer, top_layer):
            vtop_layer = vbot_layer + 1
            bport_w = int(
                self.grid.get_track_width(vbot_layer, port_widths[vbot_layer], unit_mode=True))
            tport_w = int(
                self.grid.get_track_width(vtop_layer, port_widths[vtop_layer], unit_mode=True))
            bcap_w = int(round(cap_info[vbot_layer][0] / res))
            tcap_w = int(round(cap_info[vtop_layer][0] / res))
            if bport_w < w_two_via:
                bport_w = w_single_via
            else:
                bport_w = w_two_via
            if tport_w < w_two_via:
                tport_w = w_single_via
            else:
                tport_w = w_two_via
            # port-to-port via
            vbext1, vtext1 = tuple2_to_int(
                self.grid.get_via_extensions_dim(vbot_layer, bport_w, tport_w,
                                                 unit_mode=True))
            # cap-to-port via
            vbext2 = int(self.grid.get_via_extensions_dim(vbot_layer, bcap_w, tport_w,
                                                          unit_mode=True)[0])
            # port-to-cap via
            vtext2 = int(self.grid.get_via_extensions_dim(vbot_layer, bport_w, tcap_w,
                                                          unit_mode=True)[1])

            # record extension due to via
            via_ext_dict[vbot_layer] = max(via_ext_dict[vbot_layer], vbext1, vbext2)
            via_ext_dict[vtop_layer] = max(via_ext_dict[vtop_layer], vtext1, vtext2)

        # find port locations and cap boundaries.
        port_tracks = {}
        cap_bounds = {}
        cap_exts = {}
        for cur_layer in range(bot_layer, top_layer + 1):
            # mark bounding box as used.
            # a = cur_layer
            self.mark_bbox_used(cur_layer, cap_box)

            cur_num_ports = num_ports_on_edge.get(cur_layer, 1)
            cur_port_width = port_widths[cur_layer]
            cur_port_space = self.grid.get_num_space_tracks(cur_layer, cur_port_width,
                                                            half_space=True)
            if self.grid.get_direction(cur_layer) == 'y':
                # if self.grid.get_direction(cur_layer) == cap_direction:
                cur_lower, cur_upper = cap_box.bottom_unit, cap_box.top_unit
            else:
                cur_lower, cur_upper = cap_box.left_unit, cap_box.right_unit
                # cur_lower, cur_upper = cap_box.bottom_unit, cap_box.top_unit
            # make sure adjacent layer via extension will not extend outside of cap bounding box.
            adj_via_ext = 0
            if cur_layer != bot_layer:
                adj_via_ext = via_ext_dict[cur_layer - 1]
            if cur_layer != top_layer:
                adj_via_ext = max(adj_via_ext, via_ext_dict[cur_layer + 1])
            # find track indices
            if array:
                tr_lower = self.grid.coord_to_track(cur_layer, cur_lower, unit_mode=True)
                tr_upper = self.grid.coord_to_track(cur_layer, cur_upper, unit_mode=True)
            else:
                tr_lower = self.grid.find_next_track(cur_layer, cur_lower + adj_via_ext,
                                                     tr_width=cur_port_width,
                                                     half_track=True, mode=1, unit_mode=True)
                tr_upper = self.grid.find_next_track(cur_layer, cur_upper - adj_via_ext,
                                                     tr_width=cur_port_width,
                                                     half_track=True, mode=-1, unit_mode=True)

            port_delta = cur_port_width + max(port_sp_min.get(cur_layer, 0), cur_port_space)
            if tr_lower + 2 * (cur_num_ports - 1) * port_delta >= tr_upper:
                raise ValueError('Cannot draw MOM cap; area too small.')

            ll0, lu0 = tuple2_to_int(
                self.grid.get_wire_bounds(cur_layer, tr_lower, width=cur_port_width,
                                          unit_mode=True))
            tmp = self.grid.get_wire_bounds(cur_layer,
                                            tr_lower + (cur_num_ports - 1) * port_delta,
                                            width=cur_port_width,
                                            unit_mode=True)
            ll1, lu1 = tuple2_to_int(tmp)
            tmp = self.grid.get_wire_bounds(cur_layer,
                                            tr_upper - (cur_num_ports - 1) * port_delta,
                                            width=cur_port_width,
                                            unit_mode=True)
            ul0, uu0 = tuple2_to_int(tmp)
            ul1, uu1 = tuple2_to_int(self.grid.get_wire_bounds(cur_layer, tr_upper,
                                                               width=cur_port_width,
                                                               unit_mode=True))

            # compute space from MOM cap wires to port wires
            port_w = lu0 - ll0
            lay_name = tech_info.get_layer_name(cur_layer)
            if isinstance(lay_name, tuple) or isinstance(lay_name, list):
                lay_name = lay_name[0]
            lay_type = tech_info.get_layer_type(lay_name)
            cur_margin = int(round(cap_margins[cur_layer] / res))
            cur_margin = max(cur_margin, tech_info.get_min_space(lay_type, port_w, unit_mode=True))

            lower_tracks = [tr_lower + idx * port_delta for idx in range(cur_num_ports)]
            upper_tracks = [tr_upper - idx * port_delta for idx in range(cur_num_ports - 1, -1, -1)]
            port_tracks[cur_layer] = (lower_tracks, upper_tracks)
            cap_bounds[cur_layer] = (lu1 + cur_margin, ul0 - cur_margin)
            cap_exts[cur_layer] = (ll0, uu1)

        port_tracks_x = {}
        cap_bounds_x = {}
        cap_exts_x = {}
        port_tracks_y = {}
        cap_bounds_y = {}
        cap_exts_y = {}
        if bot_direction == 'x':
            port_tracks_x = port_tracks[bot_layer]
            cap_bounds_x = cap_bounds[bot_layer]
            cap_exts_x = cap_exts[bot_layer]
            port_tracks_y = port_tracks[bot_layer + 1]
            cap_bounds_y = cap_bounds[bot_layer + 1]
            cap_exts_y = cap_exts[bot_layer + 1]
        else:
            port_tracks_x = port_tracks[bot_layer + 1]
            cap_bounds_x = cap_bounds[bot_layer + 1]
            cap_exts_x = cap_exts[bot_layer + 1]
            port_tracks_y = port_tracks[bot_layer]
            cap_bounds_y = cap_bounds[bot_layer]
            cap_exts_y = cap_exts[bot_layer]

        for cur_layer in range(bot_layer, top_layer + 1):
            if is_horizontal:
                port_tracks[cur_layer] = port_tracks_x
                cap_bounds[cur_layer] = cap_bounds_y
                cap_exts[cur_layer] = cap_exts_x
            else:
                port_tracks[cur_layer] = port_tracks_y
                cap_bounds[cur_layer] = cap_bounds_x
                cap_exts[cur_layer] = cap_exts_y

        port_dict = {}
        cap_wire_dict = {}

        # draw ports/wires

        for cur_layer in range(bot_layer, top_layer + 1):

            if cur_layer != top_layer:
                lower, upper = cap_exts[cur_layer + 1]
            if cur_layer != bot_layer:
                tmpl, tmpu = cap_exts[cur_layer - 1]
                lower = tmpl if lower is None else min(lower, tmpl)
                upper = tmpu if upper is None else max(upper, tmpu)
            assert lower is not None and upper is not None, \
                ('cur_layer is iterating and should never be equal '
                 'to both bot_layer and top_layer at the same time')

            via_ext = via_ext_dict[cur_layer]
            lower -= via_ext
            upper += via_ext
            #
            cap_lower, cap_upper = cap_bounds[cur_layer]
            cap_tot_space = cap_upper - cap_lower
            cap_w, cap_sp = cap_info[cur_layer]
            cap_w = int(round(cap_w / res))
            cap_sp = int(round(cap_sp / res))
            cap_pitch = cap_w + cap_sp
            num_cap_wires = cap_tot_space // cap_pitch
            # test = num_cap_wires % 2
            # print('test=',test)
            if num_cap_wires % 2 == 0:
                num_cap_wires = num_cap_wires
            else:
                num_cap_wires = num_cap_wires + 1
            cap_lower += (cap_tot_space - (num_cap_wires * cap_pitch - cap_sp)) // 2
            # cur_port_width = port_widths[cur_layer]
            #
            # cur_port_w = cur_port_width * track_width + via_ext * 2 * res

            if is_horizontal:  # cur_layer_dirt == 'x':
                lower_x = lower * res
                upper_x = upper * res
            else:
                lower_y = lower * res
                upper_y = upper * res
            if cur_layer == top_layer:
                print('!!top_layer=', cur_layer, 'left=', lower_x, 'right=', upper_x, 'bottom=',
                      lower_y, 'top=', upper_y)

            # '''''#5
            # draw lower and upper ports, but not here
            # lower_tracks, upper_tracks = port_tracks[cur_layer] #----

            # assign port wires to positive/negative terminals
            lpar, upar = port_parity[cur_layer]
            if lpar == upar:
                raise ValueError('Port parity must be different.')

            if is_horizontal:
                wbox = BBox(lower, cap_lower, upper, cap_lower + cap_w, res, unit_mode=True)
            else:
                wbox = BBox(cap_lower, lower, cap_lower + cap_w, upper, res, unit_mode=True)

            lay_name_list = tech_info.get_layer_name(cur_layer)
            if isinstance(lay_name_list, str):
                lay_name_list = [lay_name_list]

            # save cap wire information
            cur_rect_box = wbox
            cap_wire_dict[cur_layer] = (lpar, lay_name_list, cur_rect_box, num_cap_wires, cap_pitch)

        # '''''#1
        # draw cap wires and connect to port
        rect_list = []
        for cur_layer in range(bot_layer, top_layer + 1):
            cur_rect_list = []
            lpar, lay_name_list, cap_base_box, num_cap_wires, cap_pitch = cap_wire_dict[cur_layer]

            num_lay_names = len(lay_name_list)

            cur_port_width = port_widths[cur_layer]
            # cur_port_w = self.grid.get_track_width(cur_layer, port_widths[cur_layer], unit_mode=False)
            cur_port_w = cur_port_width * track_width + via_ext * 2 * res
            if cur_port_w < w_two_via * res:
                cur_port_w = w_single_via * res
            else:
                cur_port_w = w_two_via * res
            ori_left, ori_bottom, ori_right, ori_top = cap_base_box.left, cap_base_box.bottom, cap_base_box.right, cap_base_box.top
            
            if num_cap_wires <= 2:
                raise ValueError(
                    "There isn't enough space to draw the cap_wires (those make up the metal plates of MoM cap).\n"
                    "Try tweaking the width & height of capacitor in params.py file of the generator"
                )
            
            for idx in range(num_cap_wires):
                # draw the cap wire
                cap_lay_name = lay_name_list[idx % num_lay_names]

                if is_horizontal:
                    # meeting drc rules(min) // 2*cap_sp*res can be modified to larger num to decrease the overlap area
                    new_left = ori_left + cur_port_w + 2 * cap_sp * res
                    new_right = ori_right - cur_port_w - 2 * cap_sp * res
                    # new_right = upp_x - ori_right
                    if idx % 2 == 0:
                        cap_base_box_new = BBox(new_left, ori_bottom, ori_right, ori_top, res, unit_mode=False)
                    else:
                        cap_base_box_new = BBox(ori_left, ori_bottom, new_right, ori_top, res, unit_mode=False)
                    cap_box = cap_base_box_new.move_by(dy=cap_pitch * idx, unit_mode=True)
                    if idx == 0:
                        via_y_bottom = cap_box.bottom
                    if idx == num_cap_wires - 1:
                        via_y_top = cap_box.top
                else:
                    new_bottom = ori_bottom + cur_port_w + 2 * cap_sp * res
                    new_top = ori_top - cur_port_w - 2 * cap_sp * res
                    if idx % 2 == 0:
                        cap_base_box_new = BBox(ori_left, new_bottom, ori_right, ori_top, res, unit_mode=False)
                    else:
                        cap_base_box_new = BBox(ori_left, ori_bottom, ori_right, new_top, res, unit_mode=False)
                    cap_box = cap_base_box_new.move_by(dx=cap_pitch * idx, unit_mode=True)
                    if idx == 0:
                        via_x_left = cap_box.left
                    if idx == num_cap_wires - 1:
                        via_x_right = cap_box.right
                rect = self.add_rect(cap_lay_name, cap_box)
                cur_rect_list.append(rect)

        lower_tracks, upper_tracks = port_tracks[bot_layer]
        lower_track = lower_tracks[0]
        upper_track = upper_tracks[0]
        cap_port_w = self.grid.get_track_width(bot_layer, port_widths[bot_layer], unit_mode=False) + via_ext * 2 * res
        # --------------------------------------
        if cap_port_w < w_two_via * res:
            cap_port_w = w_single_via * res
        else:
            cap_port_w = w_two_via * res
        # --------------------------------------
        if is_horizontal:
            # left1 = track_init + lower_track * track_pitch - 0.5 * cap_port_w
            # right1 = track_init + lower_track * track_pitch + 0.5 * cap_port_w
            # left2 = track_init + upper_track * track_pitch - 0.5 * cap_port_w
            # right2 = track_init + upper_track * track_pitch + 0.5 * cap_port_w
            left1 = track_init + lower_track * track_pitch - 0.5 * cap_port_w
            if left1 == ori_left:
                left1 = left1
            else:
                left1 = ori_left
            right1 = left1 + cap_port_w
            right2 = track_init + upper_track * track_pitch + 0.5 * cap_port_w
            if right2 == ori_right:
                right2 = right2
            else:
                right2 = ori_right
            left2 = right2 - cap_port_w

            left_bbox = BBox(left1, via_y_bottom, right1, via_y_top, res, unit_mode=False)
            right_bbox = BBox(left2, via_y_bottom, right2, via_y_top, res, unit_mode=False)
        else:
            # bottom1 = track_init + lower_track * track_pitch - 0.5 * cap_port_w
            # top1 = track_init + lower_track * track_pitch + 0.5 * cap_port_w
            # bottom2 = track_init + upper_track * track_pitch - 0.5 * cap_port_w
            # top2 = track_init + upper_track * track_pitch + 0.5 * cap_port_w
            bottom1 = track_init + lower_track * track_pitch - 0.5 * cap_port_w
            if bottom1 == ori_bottom:
                bottom1 = bottom1
            else:
                bottom1 = ori_bottom
            top1 = bottom1 + cap_port_w
            top2 = track_init + upper_track * track_pitch + 0.5 * cap_port_w
            if top2 == ori_top:
                top2 = top2
            else:
                top2 = ori_top
            bottom2 = top2 - cap_port_w

            bottom_bbox = BBox(via_x_left, bottom1, via_x_right, top1, res, unit_mode=False)
            top_bbox = BBox(via_x_left, bottom2, via_x_right, top2, res, unit_mode=False)

        # Getting the track index for creating the wire array representing capacitor ports
        # (we create the wire array in the center of bottom/top ports)
        cap_lower_coord, cap_upper_coord = cap_bounds[top_layer]
        mid_coord = cap_lower_coord + (cap_upper_coord - cap_lower_coord) // 2
        port_idx = self.grid.coord_to_nearest_track(layer_id=top_layer, coord=mid_coord,
                                                    half_track=True, mode=0, unit_mode=True)
        
        for cur_layer in range(bot_layer, top_layer+1):
            # bot_layer_dirt = self.grid.get_direction(bot_layer)
            # top_layer_dirt = self.grid.get_direction(top_layer)
            
            if is_horizontal:
                direct = 'x'  # new parameter
                # self.add_rect(cur_layer_name, left_bbox)
                # self.add_rect(next_layer_name, left_bbox)
                # self.add_rect(cur_layer_name, right_bbox)
                # self.add_rect(next_layer_name, right_bbox)
                if cur_layer != top_layer:  # Don't add a via for the top layer
                    cur_layer_name = tech_info.get_layer_name(cur_layer)
                    next_layer_name = tech_info.get_layer_name(cur_layer + 1)
                    SAL_havefun3.add_via_feifeiSAL(self, left_bbox, cur_layer_name, next_layer_name, direct)
                    SAL_havefun3.add_via_feifeiSAL(self, right_bbox, cur_layer_name, next_layer_name, direct)

                # upper_terminal = self.read_wires_feifei(top_layer, upper_track, int(round(left_bbox.bottom/res)), int(round(left_bbox.top/res)), width=1, unit_mode=True)
                # lower_terminal = self.read_wires_feifei(top_layer, lower_track, int(round(left_bbox.bottom/res)), int(round(left_bbox.top/res)), width=1, unit_mode=True)
                
                if cur_layer == top_layer:  # Create capacitor ports wire array in the top layer
                    upper_terminal = WireArray(TrackID(top_layer, port_idx), int(round(right_bbox.left / res)),
                                               int(round(right_bbox.right / res)), res, unit_mode=True)
                    lower_terminal = WireArray(TrackID(top_layer, port_idx), int(round(left_bbox.left / res)),
                                               int(round(left_bbox.right / res)), res, unit_mode=True)
            else:
                direct = 'y'
                # self.add_rect(cur_layer_name, bottom_bbox)
                # self.add_rect(next_layer_name, bottom_bbox)
                # self.add_rect(cur_layer_name, top_bbox)
                # self.add_rect(next_layer_name, top_bbox)
                if cur_layer != top_layer:  # Don't add a via for the top layer
                    cur_layer_name = tech_info.get_layer_name(cur_layer)
                    next_layer_name = tech_info.get_layer_name(cur_layer + 1)
                    SAL_havefun3.add_via_feifeiSAL(self, bottom_bbox, cur_layer_name, next_layer_name, direct)
                    SAL_havefun3.add_via_feifeiSAL(self, top_bbox, cur_layer_name, next_layer_name, direct)

                # upper_terminal = self.read_wires_feifei(top_layer, upper_track, int(round(bottom_bbox.left/res)), int(round(bottom_bbox.right/res)), width=1, unit_mode=True)
                # lower_terminal = self.read_wires_feifei(top_layer, lower_track, int(round(bottom_bbox.left/res)), int(round(bottom_bbox.right/res)), width=1, unit_mode=True)
                
                if cur_layer == top_layer:
                    upper_terminal = WireArray(TrackID(top_layer, port_idx), int(round(top_bbox.bottom / res)),
                                               int(round(top_bbox.top / res)), res, unit_mode=True)
                    lower_terminal = WireArray(TrackID(top_layer, port_idx), int(round(bottom_bbox.bottom / res)),
                                               int(round(bottom_bbox.top / res)), res, unit_mode=True)

        lpar, upar = port_parity[top_layer]
        if lpar == upar:
            raise ValueError('Port parity must be different.')
        elif lpar == 0:
            plist = upper_terminal
            nlist = lower_terminal
        else:
            plist = lower_terminal
            nlist = upper_terminal
        port_dict = plist, nlist
        return port_dict  # , cap_direction
        # if return_rect:
        #     return port_dict, rect_list
        # else:
        #     return port_dict


    # -----------------

    def _draw_via_on_track_feifeiSAL(self, wlayer, box_arr, track_id, tl_unit=None,
                                     tu_unit=None, via_align=0, via_dir=True):
        # type: (str, BBoxArray, TrackID, Optional[float], Optional[float], int, bool) -> Tuple[float, float]
        """Helper method.  Draw vias on the intersection of the BBoxArray and TrackID."""
        grid = self.grid
        res = grid.resolution

        tr_layer_id = track_id.layer_id
        tr_width = track_id.width
        tr_dir = grid.get_direction(tr_layer_id)
        tr_pitch = grid.get_track_pitch(tr_layer_id)

        w_layer_id = grid.tech_info.get_layer_id(wlayer)
        w_dir = 'x' if tr_dir == 'y' else 'y'
        wbase = box_arr.base
        for sub_track_id in track_id.sub_tracks_iter(grid):
            base_idx = sub_track_id.base_index
            if w_layer_id > tr_layer_id:
                bot_layer = grid.get_layer_name(tr_layer_id, base_idx)
                top_layer = wlayer
                bot_dir = tr_dir
            else:
                bot_layer = wlayer
                top_layer = grid.get_layer_name(tr_layer_id, base_idx)
                bot_dir = w_dir
            # compute via bounding box
            tl, tu = tuple2_to_int(
                grid.get_wire_bounds(tr_layer_id, base_idx, width=tr_width, unit_mode=True))
            if tr_dir == 'x':
                via_box = BBox(wbase.left_unit, tl, wbase.right_unit, tu, res, unit_mode=True)
                nx, ny = box_arr.nx, sub_track_id.num
                spx, spy = box_arr.spx, sub_track_id.pitch * tr_pitch
                via = SAL_havefun3.add_via_feifeiSAL(self, via_box, bot_layer, top_layer, bot_dir,
                                          nx=nx, ny=ny, spx=spx, spy=spy, via_align=via_align, via_dir=via_dir)
                vtbox = via.bottom_box if w_layer_id > tr_layer_id else via.top_box
                if tl_unit is None:
                    tl_unit = vtbox.left_unit
                else:
                    tl_unit = min(tl_unit, vtbox.left_unit)
                if tu_unit is None:
                    tu_unit = vtbox.right_unit + (nx - 1) * box_arr.spx_unit
                else:
                    tu_unit = max(tu_unit, vtbox.right_unit + (nx - 1) * box_arr.spx_unit)
            else:
                via_box = BBox(tl, wbase.bottom_unit, tu, wbase.top_unit, res, unit_mode=True)
                nx, ny = sub_track_id.num, box_arr.ny
                spx, spy = sub_track_id.pitch * tr_pitch, box_arr.spy
                via = SAL_havefun3.add_via_feifeiSAL(self, via_box, bot_layer, top_layer, bot_dir,
                                          nx=nx, ny=ny, spx=spx, spy=spy, via_align=via_align, via_dir=via_dir)
                vtbox = via.bottom_box if w_layer_id > tr_layer_id else via.top_box
                if tl_unit is None:
                    tl_unit = vtbox.bottom_unit
                else:
                    tl_unit = min(tl_unit, vtbox.bottom_unit)
                if tu_unit is None:
                    tu_unit = vtbox.top_unit + (ny - 1) * box_arr.spy_unit
                else:
                    tu_unit = max(tu_unit, vtbox.top_unit + (ny - 1) * box_arr.spy_unit)
        assert tl_unit is not None and tu_unit is not None, \
            "for loop should have assigned tl_unit and tu_unit"

        return tl_unit, tu_unit

    def add_via_feifeiSAL(self,  # type: TemplateBase
                    bbox,  # type: BBox
                    bot_layer,  # type: Union[str, Tuple[str, str]]
                    top_layer,  # type: Union[str, Tuple[str, str]]
                    bot_dir,  # type: str
                    nx=1,  # type: int
                    ny=1,  # type: int
                    spx=0.0,  # type: Union[float, int]
                    spy=0.0,  # type: Union[float, int]
                    extend=True,  # type: bool
                    top_dir=None,  # type: Optional[str]
                    unit_mode=False,  # type: bool
                    via_align=0,
                    via_dir=True
                    ):
        # type: (...) -> Via_feifeiSAL
        """Adds a (arrayed) via object to the layout.

        Parameters
        ----------
        bbox : BBox
            the via bounding box, not including extensions.
        bot_layer : Union[str, Tuple[str, str]]
            the bottom layer name, or a tuple of layer name and purpose name.
            If purpose name not given, defaults to 'drawing'.
        top_layer : Union[str, Tuple[str, str]]
            the top layer name, or a tuple of layer name and purpose name.
            If purpose name not given, defaults to 'drawing'.
        bot_dir : str
            the bottom layer extension direction.  Either 'x' or 'y'.
        nx : int
            number of columns.
        ny : int
            number of rows.
        spx : Union[float, int]
            column pitch.
        spy : Union[float, int]
            row pitch.
        extend : bool
            True if via extension can be drawn outside of the box.
        top_dir : Optional[str]
            top layer extension direction.  Can force to extend in same direction as bottom.
        unit_mode : bool
            True if spx/spy are specified in resolution units.
        Returns
        -------
        via : Via
            the created via object.
        """
        # a = self.grid.tech_info
        # via = bag.layout.objects_feifei.Via_feifei(self.grid.tech_info, bbox, bot_layer, top_layer, bot_dir,
        via = Via_feifeiSAL(self.grid.tech_info, bbox, bot_layer, top_layer, bot_dir,
                         nx=nx, ny=ny, spx=spx, spy=spy, extend=extend, top_dir=top_dir,
                         unit_mode=unit_mode, via_align=via_align, via_dir=via_dir)

        self._layout.add_via(via)
        # print(bbox)
        return via

    def connect_to_tracks_feifeiSAL(self,  # type: TemplateBase
                                wire_arr_list,  # type: Union[WireArray, List[WireArray]]
                                track_id,  # type: TrackID
                                wire_lower=None,  # type: Optional[Union[float, int]]
                                wire_upper=None,  # type: Optional[Union[float, int]]
                                track_lower=None,  # type: Optional[Union[float, int]]
                                track_upper=None,  # type: Optional[Union[float, int]]
                                unit_mode=False,  # type: bool
                                min_len_mode=None,  # type: Optional[int]
                                return_wires=False,  # type: bool
                                debug=False,  # type: bool
                                via_align=0,
                                via_dir=True
                                ):
        # type: (...) -> Union[Optional[WireArray], Tuple[Optional[WireArray], List[WireArray]]]
        """Connect all given WireArrays to the given track(s).

        All given wires should be on adjacent layers of the track.

        Parameters
        ----------
        wire_arr_list : Union[WireArray, List[WireArray]]
            list of WireArrays to connect to track.
        track_id : TrackID
            TrackID that specifies the track(s) to connect the given wires to.
        wire_lower : Optional[Union[float, int]]
            if given, extend wire(s) to this lower coordinate.
        wire_upper : Optional[Union[float, int]]
            if given, extend wire(s) to this upper coordinate.
        track_lower : Optional[Union[float, int]]
            if given, extend track(s) to this lower coordinate.
        track_upper : Optional[Union[float, int]]
            if given, extend track(s) to this upper coordinate.
        unit_mode : bool
            True if track_lower/track_upper is given in resolution units.
        min_len_mode : Optional[int]
            If not None, will extend track so it satisfy minimum length requirement.
            Use -1 to extend lower bound, 1 to extend upper bound, 0 to extend both equally.
        return_wires : bool
            True to return the extended wires.
        debug : bool
            True to print debug messages.
        via_align=0,: two vias algin dirction 0, centre, <0 -x/-y, >0 +x/+y
        via_dir=True   : via direction = bottom wire direction

        Returns
        -------
        wire_arr : Union[Optional[WireArray], Tuple[Optional[WireArray], List[WireArray]]]
            WireArray representing the tracks/wires created.
            If return_wires is True, returns a Tuple[Optional[WireArray], List[WireArray]].
            If there was nothing to do, the first argument will be None.
            Otherwise, returns a WireArray.
        """

        if isinstance(wire_arr_list, WireArray):
            # convert to list.
            wire_arr_list = [wire_arr_list]
        else:
            pass

        if not wire_arr_list:
            # do nothing
            if return_wires:
                return None, []
            return None

        grid = self.grid
        res = grid.resolution

        if track_upper is not None:
            if not unit_mode:
                track_upper = int(round(track_upper / res))
            else:
                track_upper = int(track_upper)
        if track_lower is not None:
            if not unit_mode:
                track_lower = int(round(track_lower / res))
            else:
                track_lower = int(track_lower)

        # find min/max track Y coordinates
        tr_layer_id = track_id.layer_id
        wl, wu = tuple2_to_int(track_id.get_bounds(grid, unit_mode=True))
        if wire_lower is not None:
            if not unit_mode:
                wire_lower = int(round(wire_lower / res))
            else:
                wire_lower = int(wire_lower)
            wl = min(wire_lower, wl)

        if wire_upper is not None:
            if not unit_mode:
                wire_upper = int(round(wire_upper / res))
            else:
                wire_upper = int(wire_upper)
            wu = max(wire_upper, wu)

        # get top wire and bottom wire list
        top_list = []
        bot_list = []
        for wire_arr in wire_arr_list:
            cur_layer_id = wire_arr.layer_id
            if cur_layer_id == tr_layer_id + 1:
                top_list.append(wire_arr)
            elif cur_layer_id == tr_layer_id - 1:
                bot_list.append(wire_arr)
            else:
                raise ValueError(
                    'WireArray layer %d cannot connect to layer %d' % (cur_layer_id, tr_layer_id))

        # connect wires together
        top_wire_list = self.connect_wires(top_list, lower=wl, upper=wu, unit_mode=True,
                                           debug=debug)
        bot_wire_list = self.connect_wires(bot_list, lower=wl, upper=wu, unit_mode=True,
                                           debug=debug)

        # draw vias
        for w_layer_id, wire_list in ((tr_layer_id + 1, top_wire_list),
                                      (tr_layer_id - 1, bot_wire_list)):
            for wire_arr in wire_list:
                for wlayer, box_arr in wire_arr.wire_arr_iter(grid):
                    track_lower, track_upper = SAL_havefun3._draw_via_on_track_feifeiSAL(self, wlayer, box_arr, track_id,
                                                                              tl_unit=track_lower,
                                                                              tu_unit=track_upper,
                                                                              via_align=via_align, via_dir=via_dir)
        assert_msg = "track_lower/track_upper should have been set just above"
        assert track_lower is not None and track_upper is not None, assert_msg

        if min_len_mode is not None:
            # extend track to meet minimum length
            min_len = int(grid.get_min_length(tr_layer_id, track_id.width, unit_mode=True))
            # make sure minimum length is even so that middle coordinate exists
            min_len = -(-min_len // 2) * 2
            tr_len = track_upper - track_lower
            if min_len > tr_len:
                ext = min_len - tr_len
                if min_len_mode < 0:
                    track_lower -= ext
                elif min_len_mode > 0:
                    track_upper += ext
                else:
                    track_lower -= ext // 2
                    track_upper = track_lower + min_len

        # draw tracks
        result = WireArray(track_id, track_lower, track_upper, res=res, unit_mode=True)
        for layer_name, bbox_arr in result.wire_arr_iter(grid):
            self.add_rect(layer_name, bbox_arr)

        if return_wires:
            top_wire_list.extend(bot_wire_list)
            return result, top_wire_list
        else:
            return result

    # -----------------------------------------






class Via_feifeiSAL(Arrayable):
    """A layout via, with optional arraying parameters for SAL.

    Parameters
    ----------
    tech : bag.layout.core.TechInfo
        the technology class used to calculate via information.
    bbox : bag.layout.util.BBox or bag.layout.util.BBoxArray
        the via bounding box, not including extensions.
        If this is a BBoxArray, the BBoxArray's arraying parameters are used.
    bot_layer : str or (str, str)
        the bottom layer name, or a tuple of layer name and purpose name.
        If purpose name not given, defaults to 'drawing'.
    top_layer : str or (str, str)
        the top layer name, or a tuple of layer name and purpose name.
        If purpose name not given, defaults to 'drawing'.
    bot_dir : str
        the bottom layer extension direction.  Either 'x' or 'y'.
    nx : int
        arraying parameter.  Number of columns.
    ny : int
        arraying parameter.  Mumber of rows.
    spx : float
        arraying parameter.  Column pitch.
    spy : float
        arraying parameter.  Row pitch.
    extend : bool
        True if via extension can be drawn outside of bounding box.
    top_dir : Optional[str]
        top layer extension direction.  Can force to extend in same direction as bottom.
    unit_mode : bool
        True if array pitches are given in resolution units.
    """
    # from inverter_py.SAL_havefun2 import TechInfo
    # from bag.layout.tech import TechInfoConfig
    def __init__(self, tech, bbox, bot_layer, top_layer, bot_dir,
                 nx=1, ny=1, spx=0, spy=0, extend=True, top_dir=None, unit_mode=False,
                 via_align=0, via_dir=True):
        if isinstance(bbox, BBoxArray):
            self._bbox = bbox.base
            Arrayable.__init__(self, tech.resolution, nx=bbox.nx, ny=bbox.ny,
                               spx=bbox.spx_unit, spy=bbox.spy_unit, unit_mode=True)

        else:
            self._bbox = bbox
            Arrayable.__init__(self, tech.resolution, nx=nx, ny=ny, spx=spx, spy=spy,
                               unit_mode=unit_mode)

        # python 2/3 compatibility: convert raw bytes to string.
        bot_layer = bag.io.fix_string(bot_layer)
        top_layer = bag.io.fix_string(top_layer)
        # print(top_layer)
        if isinstance(bot_layer, str):
            bot_layer = (bot_layer, 'drawing')
        if isinstance(top_layer, str):
            top_layer = (top_layer, 'drawing')
            # print(top_layer)
        self._tech = tech
        self._bot_layer = bot_layer[0], bot_layer[1]
        self._top_layer = top_layer[0], top_layer[1]
        self._bot_dir = bot_dir
        self._top_dir = top_dir
        self._extend = extend
        self._info = TechInfoConfig_SAL.get_via_info_feifei(TechInfoConfig_SAL, self._bbox, bot_layer, top_layer, bot_dir,
                                             top_dir=top_dir, extend=extend, via_align=via_align, via_dir=via_dir)
        if self._info is None:
            raise ValueError('Cannot make via with bounding box %s' % self._bbox)

    def _update(self):
        """Update via parameters."""
        self._info = TechInfoConfig_SAL.get_via_info_feifei(TechInfoConfig_SAL, self.bbox, self.bot_layer, self.top_layer,
                                             self.bottom_direction, top_dir=self.top_direction,
                                             extend=self.extend, via_align=self.via_align, via_dir=self.via_dir)

    @property
    def top_box(self):
        # type: () -> BBox
        """the top via layer bounding box."""
        return self._info['top_box']

    @property
    def bottom_box(self):
        # type: () -> BBox
        """the bottom via layer bounding box."""
        return self._info['bot_box']

    @property
    def bot_layer(self):
        """The bottom via (layer, purpose) pair."""
        return self._bot_layer

    @property
    def top_layer(self):
        """The top via layer."""
        return self._top_layer

    @property
    def bottom_direction(self):
        """the bottom via extension direction."""
        return self._bot_dir

    @bottom_direction.setter
    def bottom_direction(self, new_bot_dir):
        """Sets the bottom via extension direction."""
        self.check_destroyed()
        self._bot_dir = new_bot_dir
        self._update()

    @property
    def top_direction(self):
        """the bottom via extension direction."""
        if not self._top_dir:
            return 'x' if self._bot_dir == 'y' else 'y'
        return self._top_dir

    @top_direction.setter
    def top_direction(self, new_top_dir):
        """Sets the bottom via extension direction."""
        self.check_destroyed()
        self._top_dir = new_top_dir
        self._update()

    @property
    def extend(self):
        """True if via extension can grow beyond bounding box."""
        return self._extend

    @extend.setter
    def extend(self, new_val):
        self._extend = new_val

    @property
    def bbox(self):
        """The via bounding box not including extensions."""
        return self._bbox

    @property
    def bbox_array(self):
        """The via bounding box array, not including extensions.

        Returns
        -------
        barr : :class:`bag.layout.util.BBoxArray`
            the BBoxArray representing this (Arrayed) rectangle.
        """
        return BBoxArray(self._bbox, nx=self.nx, ny=self.ny, spx=self.spx_unit,
                         spy=self.spy_unit, unit_mode=True)

    @bbox.setter
    def bbox(self, new_bbox):
        """Sets the via bounding box.  Will redraw the via."""
        self.check_destroyed()
        if not new_bbox.is_physical():
            raise ValueError('Bounding box %s is not physical' % new_bbox)
        self._bbox = new_bbox
        self._update()

    @property
    def content(self):
        """A dictionary representation of this via."""
        via_params = self._info['params']
        content = ViaInfo(self._tech.resolution, **via_params)

        if self.nx > 1 or self.ny > 1:
            content['arr_nx'] = self.nx
            content['arr_ny'] = self.ny
            content['arr_spx'] = self.spx
            content['arr_spy'] = self.spy

        return content

    def move_by(self, dx=0, dy=0, unit_mode=False):
        # type: (ldim, ldim, bool) -> None
        """Move this path by the given amount.

        Parameters
        ----------
        dx : float
            the X shift.
        dy : float
            the Y shift.
        unit_mode : bool
            True if shifts are given in resolution units.
        """
        self._bbox = self._bbox.move_by(dx=dx, dy=dy, unit_mode=unit_mode)
        self._info['top_box'] = self._info['top_box'].move_by(dx=dx, dy=dy, unit_mode=unit_mode)
        self._info['bot_box'] = self._info['bot_box'].move_by(dx=dx, dy=dy, unit_mode=unit_mode)
        self._info['params']['loc'] = [self._bbox.xc, self._bbox.yc]

    def transform(self, loc=(0, 0), orient='R0', unit_mode=False, copy=False):
        # type: (Tuple[ldim, ldim], str, bool, bool) -> Figure
        """Transform this figure."""
        new_box = self._bbox.transform(loc=loc, orient=orient, unit_mode=unit_mode)
        if copy:
            return Via_feifeiSAL(self._tech, new_box, self._bot_layer, self._top_layer, self._bot_dir,
                       nx=self.nx, ny=self.ny, spx=self.spx_unit, spy=self.spy_unit,
                       unit_mode=True, via_align=self.via_align, via_dir=self.via_dir)
        else:
            self._bbox = new_box
            self._info['top_box'] = self._info['top_box'].transform(loc=loc, orient=orient,
                                                                    unit_mode=unit_mode)
            self._info['bot_box'] = self._info['bot_box'].transform(loc=loc, orient=orient,
                                                                    unit_mode=unit_mode)
            self._info['params']['loc'] = [self._bbox.xc, self._bbox.yc]








# class TechInfoConfig_SAL:
class TechInfoConfig_SAL:#(TechInfo, metaclass=abc.ABCMeta):
    """An implementation of TechInfo that implements most methods with a technology file."""

    # TODO: clean up this legacy code
    # NOTE: for now, we inject the BAG2_technology_definition based tech_parmas here,
    #       as we don't want a tech-specific, duplicate YAML in data/tech_params.yaml
    @classmethod
    def init_tech_info(cls, tech_info: bag.layout.TechInfo):
        cls.config = tech_info.process_config

        # _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('data', 'tech_params.yaml'))
        # with open(_yaml_file, 'r') as f:
        #     config = yaml.load(f, Loader=yaml.FullLoader)
        #     print(config)

    # TechInfo.__init__(self, config['resolution'], config['layout_unit'],
        #                   config['tech_lib'], tech_params)
    @abc.abstractmethod
    def get_via_em_specs(self, via_name, bm_layer, tm_layer, via_type='square',
                         bm_dim=(-1, -1), tm_dim=(-1, -1), array=False, **kwargs):
        return float('inf'), float('inf'), float('inf')


    @abc.abstractmethod
    def get_via_arr_enc(self, vname, vtype, mtype, mw_unit, is_bot):
        # type: (...) -> Tuple[Optional[List[Tuple[int, int]]], Optional[Callable[[int, int], bool]]]
        return None, None

    def get_via_types(self, bmtype, tmtype):
        default = [('square', 1), ('vrect', 2), ('hrect', 2)]
        if 'via_type_order' in self.config:
            table = self.config['via_type_order']
            return table.get((bmtype, tmtype), default)
        return default

    def get_layer_name(self, layer_id):
        # type: (int) -> str
        name_dict = self.config['layer_name']
        return name_dict[layer_id]

    def get_layer_id(self, layer_name):
        # type: (str) -> int
        for key, val in self.config['layer_name'].items():
            if val == layer_name:
                return key
        raise ValueError('Unknown layer: %s' % layer_name)

    def get_layer_type(self, layer_name):
        # type: (str) -> str
        type_dict = self.config['layer_type']
        return type_dict[layer_name]

    def get_via_name(self, bot_layer_id):
        # type: (int) -> str
        return self.config['via_name'][bot_layer_id]

    def get_via_id(self, bot_layer, top_layer):
        # type: (str, str) -> str
        return self.config['via_id'][(bot_layer, top_layer)]

    def get_via_drc_info(self, vname, vtype, mtype, mw_unit, is_bot):
        via_config = self.config['via']
        if vname not in via_config:
            raise ValueError('Unsupported vname %s' % vname)

        via_config = via_config[vname]
        if vtype.startswith('vrect') and vtype not in via_config:
            # trying vertical rectangle via, but it does not exist,
            # so try rotating horizontal rectangle instead
            rotate = True
            vtype2 = 'hrect' + vtype[5:]
        else:
            rotate = False
            vtype2 = vtype
        if vtype2 not in via_config:
            raise ValueError('Unsupported vtype %s' % vtype2)

        via_config = via_config[vtype2]

        dim = via_config['dim']
        sp = via_config['sp']
        sp2_list = via_config.get('sp2', None)
        sp3_list = via_config.get('sp3', None)
        sp6_list = via_config.get('sp6', None)

        if not is_bot or via_config['bot_enc'] is None:
            enc_data = via_config['top_enc']
        else:
            enc_data = via_config['bot_enc']

        enc_w_list = enc_data['w_list']
        enc_list = enc_data['enc_list']

        enc_cur = []
        for mw_max, enc in zip(enc_w_list, enc_list):
            if mw_unit <= mw_max:
                enc_cur = enc
                break

        arr_enc, arr_test_tmp = TechInfoConfig_SAL.get_via_arr_enc(self, vname, vtype, mtype, mw_unit, is_bot)
        arr_test = arr_test_tmp

        if rotate:
            sp = sp[1], sp[0]
            dim = dim[1], dim[0]
            enc_cur = [(yv, xv) for xv, yv in enc_cur]
            if sp2_list is not None:
                sp2_list = [(spy, spx) for spx, spy in sp2_list]
            if sp3_list is not None:
                sp3_list = [(spy, spx) for spx, spy in sp3_list]
            if sp6_list is not None:
                sp6_list = [(spy, spx) for spx, spy in sp6_list]
            if arr_enc is not None:
                arr_enc = [(yv, xv) for xv, yv in arr_enc]
            if arr_test_tmp is not None:
                def arr_test(nrow, ncol):
                    return arr_test_tmp(ncol, nrow)

        return sp, sp2_list, sp3_list, sp6_list, dim, enc_cur, arr_enc, arr_test

    def _space_helper(self, config_name, layer_type, width):
        sp_min_config = self.config[config_name]
        if layer_type not in sp_min_config:
            raise ValueError('Unsupported layer type: %s' % layer_type)

        sp_min_config = sp_min_config[layer_type]
        w_list = sp_min_config['w_list']
        sp_list = sp_min_config['sp_list']

        for w, sp in zip(w_list, sp_list):
            if width <= w:
                return sp
        return None


    def _via_better(self, mdim_list1, mdim_list2):
        """Returns true if the via in mdim_list1 has smaller area compared with via in mdim_list2"""
        res = self._resolution
        better = False
        for mdim1, mdim2 in zip(mdim_list1, mdim_list2):
            area1 = int(round(mdim1[0] / res)) * int(round(mdim1[1] / res))
            area2 = int(round(mdim2[0] / res)) * int(round(mdim2[1] / res))
            if area1 < area2:
                better = True
            elif area1 > area2:
                return False
        return better









    def get_best_via_array_feifei(self, vname, bmtype, tmtype, bot_dir, top_dir, w, h, extend):
        """Maximize the number of vias in the given bounding box.
        ---------------------------------------square vias only !!!------------------------------------
        Parameters
        ----------
        vname : str
            the via type name.
        bmtype : str
            the bottom metal type name.
        tmtype : str
            the top metal type name.
        bot_dir : str
            the bottom wire direction.  Either 'x' or 'y'.
        top_dir : str
            the top wire direction.  Either 'x' or 'y'.
        w : float
            width of the via array bounding box, in layout units.
        h : float
            height of the via array bounding box, in layout units.
        extend : bool
            True if via can extend beyond bounding box.

        Returns
        -------
        best_nxy : Tuple[int, int]
            optimal number of vias per row/column.
        best_mdim_list : List[Tuple[int, int]]
            a list of bottom/top layer width/height, in resolution units.
        vtype : str
            the via type to draw, square/hrect/vrect/etc.
        vdim : Tuple[int, int]
            the via width/height, in resolution units.
        via_space : Tuple[int, int]
            the via horizontal/vertical spacing, in resolution units.
        via_arr_dim : Tuple[int, int]
            the via array width/height, in resolution units.
        """
        # This entire optimization routine relies on the bounding box being measured integer units
        res = self.config['resolution']
        w = int(round(w / res))
        h = int(round(h / res))
        # print(w,h)
        # Depending on the routing direction of the metal, the provided width/height of the
        # bounding box may correspond to either the x direction or y direction.
        if bot_dir == 'x':
            bb, be = h, w
        else:
            bb, be = w, h
        if top_dir == 'x':
            tb, te = h, w
        else:
            tb, te = w, h

        # Initialize variables that will hold optimal via size at the end of the algorithm
        best_num = None
        best_nxy = [-1, -1]
        best_mdim_list = None
        best_type = None
        best_vdim = None
        best_sp = None
        best_adim = None

        # Perform via optimization algorithm for all available via types. Some technologies have
        # both square and rectangular via types, which can be used in different situations. Each
        # via_type has a weight which signifies a preference for choosing one type over another
        via_type_list = TechInfoConfig_SAL.get_via_types(self, bmtype, tmtype)
        # print(via_type_list)
        via_square = via_type_list[0]
        # via_type_list = [via_square,via_square,via_square]
        via_type_list = [via_square]
        # print(via_type_list)

        for vtype, weight in via_type_list:
            # Extract via drc information from the loaded tech yaml file. Some drc info is optional
            # so catch ValueErrors from missing info and move on
            try:
                # get space and enclosure rules for top and bottom layer
                bot_drc_info = TechInfoConfig_SAL.get_via_drc_info(self, vname, vtype, bmtype, bb, True)

                top_drc_info = TechInfoConfig_SAL.get_via_drc_info(self, vname, vtype, tmtype, tb, False)

                sp, sp2_list, sp3_list, sp6_list, dim, encb, arr_encb, arr_testb = bot_drc_info
                _, _, _, _, _, enct, arr_enct, arr_testt = top_drc_info
                # print(sp[0], '+', dim[0], '+', dim[0], '+', encb[2][0], '+', encb[2][0])
                w_bk = sp[0] + dim[0] + dim[0] + encb[0][0] + encb[0][0]
                w_bk = w_bk * res
                # print(w,h,w_bk)
            except ValueError:
                continue
            # optional sp2/sp3 rules enable different spacing rules for via arrays with 2 or 3 neighbors
            if sp2_list is None:
                sp2_list = [sp]
            if sp3_list is None:
                sp3_list = sp2_list
            if sp6_list is None:
                sp6_list = sp3_list
            # print('sp2_list', sp2_list)
            # Get minimum possible spacing between vias
            spx_min, spy_min = sp
            for high_sp_list in (sp2_list, sp3_list, sp6_list):
                for high_spx, high_spy in high_sp_list:
                    spx_min = min(spx_min, high_spx)
                    spy_min = min(spy_min, high_spy)
                    # print(high_spy)
            # Get minimum possible enclosure size for top or bottom layers
            extx = 0
            exty = 0

            for enc in chain(encb, enct):
                extx = min(extx, enc[0])
                exty = min(exty, enc[1])

            # Allocate area in the bounding box for minimum enclosure, then find
            # maximum number of vias that can fit in the remaining area with the minimum spacing
            if np.isinf(spx_min):
                nx_max = 1 if (w - 2 * extx) // dim[0] else 0
            else:
                nx_max = (w + spx_min - 2 * extx) // (dim[0] + spx_min)
            if np.isinf(spy_min):
                ny_max = 1 if (h - 2 * exty) // dim[1] else 0
            else:
                ny_max = (h + spy_min - 2 * exty) // (dim[1] + spy_min)

            # Theoretically any combination of via array size from (1, 1) to (nx_max, ny_max) may actually
            # work within the given bound box. Here we enumerate a list all of these possible via combinations
            # starting from the max via number
            # print((w + spx_min - 2 * extx) // (dim[0] + spx_min))
            # ------------------------

            # if nx_max==0:
            #     nx_max = nx_max+1
            # else: nx_max = nx_max
            # if ny_max==0:
            #     ny_max = ny_max+1
            # else: ny_max = ny_max
            # --------------------------
            nxy_list = [(a * b, a, b) for a in range(1, nx_max + 1) for b in range(1, ny_max + 1)]
            nxy_list = sorted(nxy_list, reverse=True)


            # Initialize variables that will hold the best working via array size for this via type
            opt_nxy = None
            opt_mdim_list = None
            opt_adim = None
            opt_sp = None

            # This looping procedure will iterate over all possible via array configurations and select
            # one that maximizes the number of vias while meeting all rules
            for num, nx, ny in nxy_list:
                # Determine whether we should be using sp/sp2/sp3 rules for the current via configuration
                if (nx == 1 and ny >= 1) or (nx >= 1 and ny == 1):
                    sp_combo = [sp]
                elif nx == 2 and ny == 2:
                    sp_combo = sp2_list
                elif nx >= 6 and ny >= 6:
                    sp_combo = sp6_list
                else:
                    sp_combo = sp3_list

                # DRC rules can typically be satisfied with a number of different spacing rules, so here we
                # iterate over each to find the best one. Note that since we break out of the loop immediately upon
                # finding a valid via configuration, this code prioritizes spacing rules that are early on in the list
                for spx, spy in sp_combo:
                    # Compute a bounding box for the via array without the enclosure
                    w_arr = dim[0] if nx == 1 else nx * (spx + dim[0]) - spx
                    h_arr = dim[1] if ny == 1 else ny * (spy + dim[1]) - spy
                    mdim_list = [None, None]



                    # Loop over all possible enclosure types and check whether this via configuration satisfies
                    # one of them for both the bottom metal and top metal
                    for idx, (mdir, tot_enc_list, arr_enc, arr_test) in \
                            enumerate([(bot_dir, encb, arr_encb, arr_testb),
                                       (top_dir, enct, arr_enct, arr_testt)]):

                        # arr_test is a function that takes an array size as input and returns a boolean. If its
                        # is true the array size is valid and is added to the list of valid enclosures
                        if arr_test is not None and arr_test(ny, nx):
                            tot_enc_list = tot_enc_list + arr_enc

                        # If the routing direction is y, start by computing x-direction enclosure. ext_dim
                        # corresponds to x-direction. Vice-versa if the routing direction is x
                        if mdir == 'y':
                            enc_idx = 0
                            enc_dim = w_arr
                            ext_dim = h_arr
                            dim_lim = w
                            max_ext_dim = h
                        else:
                            enc_idx = 1
                            enc_dim = h_arr
                            ext_dim = w_arr
                            dim_lim = h
                            max_ext_dim = w

                        # Initialize variable to hold opposite direction enclosure size
                        min_ext_dim = None

                        # This loop selects the minimum opposite direction size that satisfies the enclosure
                        # rules
                        for enc in tot_enc_list:
                            cur_ext_dim = ext_dim + 2 * enc[1 - enc_idx]
                            # Check that the enclosure rule is satisfied. If extend is true, this passing enclosure
                            # size can exceed the maximum size set by the user provided bounding box
                            if (enc[enc_idx] * 2 + enc_dim <= dim_lim) and (extend or cur_ext_dim <= max_ext_dim):
                                # Select the minimum of all enclosures in the non-routing direction that satisfies
                                # the enclosure rules
                                if min_ext_dim is None or min_ext_dim > cur_ext_dim:
                                    min_ext_dim = cur_ext_dim

                        # If none of the enclosures in the list meet the rules, the current spacing rules cannot
                        # be used to create a valid via, so we continue on to the next set of spacing rules
                        if min_ext_dim is None:
                            break
                        # Otherwise record the computed via dimensions that pass all checks
                        else:
                            min_ext_dim = max(min_ext_dim, max_ext_dim)
                            mdim_list[idx] = [min_ext_dim, min_ext_dim]
                            mdim_list[idx][enc_idx] = dim_lim

                    # If we've found a valid via configuration immediately break out of the loop
                    if mdim_list[0] is not None and mdim_list[1] is not None:
                        # passed
                        opt_mdim_list = mdim_list
                        opt_nxy = (nx, ny)
                        opt_adim = (w_arr, h_arr)
                        opt_sp = (spx, spy)
                        break

                # If we've found a valid via array size immediately break out of the loop
                if opt_nxy is not None:
                    break

            # Select the best via out of all the passing via types. Vias are selected by choosing the
            # highest 'best_num'. This is calculated by multiplying the via array size by the via weight
            # Ties between vias are broken by minimizing drawn via area
            if opt_nxy is not None:
                opt_num = weight * opt_nxy[0] * opt_nxy[1]
                if (best_num is None or opt_num > best_num or
                        (opt_num == best_num and self._via_better(best_mdim_list, opt_mdim_list))):
                    best_num = opt_num
                    best_nxy = opt_nxy
                    best_mdim_list = opt_mdim_list
                    best_type = vtype
                    best_vdim = dim
                    best_sp = opt_sp
                    best_adim = opt_adim

                    # print(best_num, 'done')

        if best_num is None:
            return None
        # print(best_num, best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim)
        return w_bk, best_num, best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim

    def get_best_via_array_feifei2(self, vname, bmtype, tmtype, bot_dir, top_dir, w, h, extend, via_dir=True):
        # via_dir = True means via_dir = bot_dir
        result_round1 = TechInfoConfig_SAL.get_best_via_array_feifei(self, vname, bmtype, tmtype, bot_dir, top_dir, w, h, extend)
        w_bk, best_num, best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim = result_round1
        if best_num == 1:
            if via_dir == True:
                print('ViaDir = bot_dir')
                if bot_dir == 'x':
                    w = w_bk
                else:
                    h = w_bk
            else:
                print('ViaDir = top_dir')
                if bot_dir == 'x':
                    h = w_bk
                else:
                    w = w_bk

            result_round2 = TechInfoConfig_SAL.get_best_via_array_feifei(self, vname, bmtype, tmtype, bot_dir, top_dir, w, h, extend)
            w_bk, best_num, best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim = result_round2
        else: w_bk, best_num, best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim = result_round1
        # print(best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim)
        return best_nxy, best_mdim_list, best_type, best_vdim, best_sp, best_adim

    def get_via_info_feifei(self, bbox, bot_layer, top_layer, bot_dir, bot_len=-1, top_len=-1,
                     extend=True, top_dir=None, via_align=0, via_dir=True, **kwargs):
        """Create a via on the routing grid given the bounding box.

        Parameters
        ----------
        bbox : bag.layout.util.BBox
            the bounding box of the via.
        bot_layer : Union[str, Tuple[str, str]]
            the bottom layer name, or a tuple of layer name and purpose name.
            If purpose name not given, defaults to 'drawing'.
        top_layer : Union[str, Tuple[str, str]]
            the top layer name, or a tuple of layer name and purpose name.
            If purpose name not given, defaults to 'drawing'.
        bot_dir : str
            the bottom layer extension direction.  Either 'x' or 'y'
        bot_len : float
            length of bottom wire connected to this Via, in layout units.
            Used for length enhancement EM calculation.
        top_len : float
            length of top wire connected to this Via, in layout units.
            Used for length enhancement EM calculation.
        extend : bool
            True if via extension can be drawn outside of bounding box.
        top_dir : Optional[str]
            top layer extension direction.  Can force to extend in same direction as bottom.
        **kwargs :
            optional parameters for EM rule calculations, such as nominal temperature,
            AC rms delta-T, etc.
        via_dir=True means via_dirction = bottom dirction
        Returns
        -------
        info : dict[string, any]
            A dictionary of via information, or None if no solution.  Should have the following:

            resistance : float
                The total via array resistance, in Ohms.
            idc : float
                The total via array maximum allowable DC current, in Amperes.
            iac_rms : float
                The total via array maximum allowable AC RMS current, in Amperes.
            iac_peak : float
                The total via array maximum allowable AC peak current, in Amperes.
            params : dict[str, any]
                A dictionary of via parameters.
            top_box : bag.layout.util.BBox
                the top via layer bounding box, including extensions.
            bot_box : bag.layout.util.BBox
                the bottom via layer bounding box, including extensions.

        """
        # remove purpose
        if isinstance(bot_layer, tuple):
            bot_layer = bot_layer[0]
        if isinstance(top_layer, tuple):
            top_layer = top_layer[0]
        bot_layer = bag.io.fix_string(bot_layer)
        top_layer = bag.io.fix_string(top_layer)

        bot_id = TechInfoConfig_SAL.get_layer_id(self, bot_layer)
        bmtype = TechInfoConfig_SAL.get_layer_type(self, bot_layer)
        tmtype = TechInfoConfig_SAL.get_layer_type(self, top_layer)
        vname = TechInfoConfig_SAL.get_via_name(self, bot_id)

        if not top_dir:
            top_dir = 'x' if bot_dir == 'y' else 'y'
        # ------------------------------------*********************************************
        # via_result = self.get_best_via_array(vname, bmtype, tmtype, bot_dir, top_dir,
        #                                      bbox.width, bbox.height, extend)

        via_result = TechInfoConfig_SAL.get_best_via_array_feifei2(self, vname, bmtype, tmtype, bot_dir, top_dir,
                                                     bbox.width, bbox.height, extend, via_dir)
        # print('via_result',via_result)
        if via_result is None:
            # no solution found
            return None

        (nx, ny), mdim_list, vtype, vdim, (spx, spy), (warr_norm, harr_norm) = via_result
        # print(mdim_list)
        res = self.config['resolution']
        xc_norm = bbox.xc_unit
        yc_norm = bbox.yc_unit
        # print(xc_norm,yc_norm, bbox)

        wbot_norm = mdim_list[0][0]
        hbot_norm = mdim_list[0][1]
        wtop_norm = mdim_list[1][0]
        htop_norm = mdim_list[1][1]
        # print(wbot_norm, hbot_norm, wtop_norm, htop_norm)

        # OpenAccess Via can't handle even + odd enclosure, so we truncate.
        enc1_x = (wbot_norm - warr_norm) // 2 * res
        enc1_y = (hbot_norm - harr_norm) // 2 * res
        enc2_x = (wtop_norm - warr_norm) // 2 * res
        enc2_y = (htop_norm - harr_norm) // 2 * res
        # print(enc1_x, enc1_y,enc2_x,enc2_y)

        # compute EM rule dimensions
        if bot_dir == 'x':
            bw, tw = hbot_norm * res, wtop_norm * res
        else:
            bw, tw = wbot_norm * res, htop_norm * res
        # -----------------------------------------------
        via_align = via_align
        XC = xc_norm
        YC = yc_norm
        if (nx, ny) == (2,1):
            if via_align == 0:
                XC = xc_norm
                YC = yc_norm
            elif via_align >= 0:
                XC = xc_norm + (wbot_norm - bbox.width//res)//2
                YC = yc_norm + (hbot_norm - bbox.height//res)//2
            elif via_align <= 0:
                XC = xc_norm - (wbot_norm - bbox.width//res)//2
                YC = yc_norm - (hbot_norm - bbox.height//res)//2
        elif (nx, ny) == (1,2):
            if via_align == 0:
                XC = xc_norm
                YC = yc_norm
            elif via_align >= 0:
                XC = xc_norm + (wbot_norm - bbox.width//res)//2
                YC = yc_norm + (hbot_norm - bbox.height//res)//2
            elif via_align <= 0:
                XC = xc_norm - (wbot_norm - bbox.width//res)//2
                YC = yc_norm - (hbot_norm - bbox.height//res)//2

        bot_xl_norm = XC - wbot_norm // 2
        bot_yb_norm = YC - hbot_norm // 2
        top_xl_norm = XC - wtop_norm // 2
        top_yb_norm = YC - htop_norm // 2


        # original
        # bot_xl_norm = xc_norm - wbot_norm // 2
        # bot_yb_norm = yc_norm - hbot_norm // 2
        # top_xl_norm = xc_norm - wtop_norm // 2
        # top_yb_norm = yc_norm - htop_norm // 2
        # print(bbox.width, wbot_norm)

        bot_box = BBox(bot_xl_norm, bot_yb_norm, bot_xl_norm + wbot_norm,
                       bot_yb_norm + hbot_norm, res, unit_mode=True)
        top_box = BBox(top_xl_norm, top_yb_norm, top_xl_norm + wtop_norm,
                       top_yb_norm + htop_norm, res, unit_mode=True)

        # original
        # print(bot_box)
        idc, irms, ipeak = TechInfoConfig_SAL.get_via_em_specs(self, vname, bot_layer, top_layer, via_type=vtype,
                                                 bm_dim=(bw, bot_len), tm_dim=(tw, top_len),
                                                 array=nx > 1 or ny > 1, **kwargs)

        # if nx * ny ==2:
        #     XC = xc_norm + (wbot_norm - bbox.width//res)//2
        #     YC = yc_norm + (hbot_norm - bbox.height//res)//2
        # else:
        #     XC = xc_norm
        #     YC = yc_norm


        params = {'id': TechInfoConfig_SAL.get_via_id(self, bot_layer, top_layer),
                  # 'loc': (xc_norm * res, yc_norm * res),
                  'loc': ( XC* res, YC * res),
                  'orient': 'R0',
                  'num_rows': ny,
                  'num_cols': nx,
                  'sp_rows': spy * res,
                  'sp_cols': spx * res,
                  # increase left/bottom enclusion if off-center.
                  'enc1': [enc1_x, enc1_x, enc1_y, enc1_y],
                  'enc2': [enc2_x, enc2_x, enc2_y, enc2_y],
                  'cut_width': vdim[0] * res,
                  'cut_height': vdim[1] * res,
                  }

        ntot = nx * ny
        # print(ntot)
        return dict(
            resistance=0.0,
            idc=idc * ntot,
            iac_rms=irms * ntot,
            iac_peak=ipeak * ntot,
            params=params,
            top_box=top_box,
            bot_box=bot_box,
        )




