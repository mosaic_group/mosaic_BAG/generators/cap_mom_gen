#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *
from sal.row import ChannelType


@dataclass
class TrackInfo:
    """
    Parameters to construct a TrackID object
    """
    index: int
    width: Union[float, int]


@dataclass
class cap_mom_layout_params(LayoutParamsBase):
    """
    Parameter class for cap_mom_gen

    Args:
    ----

    bot_layer: int
        MOM cap bottom layer.

    top_layer: int
        MOM cap top layer.

    width: int
        MOM cap width, in resolution units.

    height: int
        MOM cap height, in resolution units.

    margin: int
        margin between cap and boundary, in resolution units.

    in_tid: Optional[TrackInfo]
        Input TrackID information.

    out_tid: Optional[TrackInfo]
        Output TrackID information.

    port_tr_w: int
        MOM cap port track width, in number of tracks.

    options: Optional[Dict]
        MOM cap layout options.

    fill_config: Optional[Dict[int, Tuple[int, int, int, int]]]
        Fill configuration dictionary.  If not None, quantize to fill grid.

    fill_dummy: bool
        True to draw dummy fill.

    fill_pitch: int
        dummy fill pitch.

    mos_type: ChannelType
        dummy fill transistor type.

    threshold: str
        dummy fill threshold.

    half_blk_x: bool
        True to allow half horizontal blocks.

    half_blk_y: bool
        True to allow half vertical blocks.

    sub_name: Optional[str]
        type of substrate

    show_pins: bool
        True to show pin labels.
    """

    bot_layer: int
    top_layer: int
    width: int
    height: int
    margin: int
    in_tid: Optional[TrackInfo]
    out_tid: Optional[TrackInfo]
    port_tr_w: int
    options: Optional[Dict]
    fill_config: Optional[Dict[int, Tuple[int, int, int, int]]]
    fill_dummy: bool
    fill_pitch: int
    mos_type: ChannelType
    threshold: str
    half_blk_x: bool
    half_blk_y: bool
    sub_name: Optional[str]
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> cap_mom_layout_params:
        return cap_mom_layout_params(
            bot_layer=3,
            top_layer=5,
            width=512,  # 576, #550,
            height=480,
            margin=0,
            in_tid=None,
            out_tid=None,
            port_tr_w=1,
            options=None,
            fill_config=None,
            fill_dummy=False,
            fill_pitch=2,
            mos_type=ChannelType.N,
            threshold='standard',
            half_blk_x=True,
            half_blk_y=True,
            sub_name=None,
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> cap_mom_layout_params:
        return cap_mom_layout_params(
            bot_layer=3,
            top_layer=5,
            width=512 * 5,   # NOTE: increased width required for sky130A to be LVS clean
            height=480 * 5,  # NOTE: increased height required for sky130A to be LVS clean
            margin=0,
            in_tid=None,
            out_tid=None,
            port_tr_w=1,
            options=None,
            fill_config=None,
            fill_dummy=False,
            fill_pitch=2,
            mos_type=ChannelType.N,
            threshold='standard',
            half_blk_x=True,
            half_blk_y=True,
            sub_name="VSS",
            show_pins=True,
        )


@dataclass
class cap_mom_params(GeneratorParamsBase):
    layout_parameters: cap_mom_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> cap_mom_params:
        return cap_mom_params(
            layout_parameters=cap_mom_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
