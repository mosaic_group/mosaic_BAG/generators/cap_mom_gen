import os
from typing import *
from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/cap_mom_templates',
                         'netlist_info', 'cap_mom.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library cap_mom_templates cell cap_mom.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            res_in_info='input metal resistor information.',
            res_out_info='output metal resistor information.',
            sub_name='substrate name.  Empty string to disable.',
        )

    def design(self,
               res_in_info: Tuple[int, float, float],
               res_out_info: Tuple[int, float, float],
               sub_name: str):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        self.instances['XP'].design(w=res_in_info[1], l=res_in_info[2], layer=res_in_info[0], intent=res_in_info[3])
        self.instances['XN'].design(w=res_out_info[1], l=res_out_info[2], layer=res_out_info[0], intent=res_out_info[3])

        if not sub_name:
            # delete substrate pin
            self.delete_instance('XSUPCONN')
            self.remove_pin('VSS')
        elif sub_name != 'VSS':
            self.rename_pin('VSS', sub_name)
            self.reconnect_instance_terminal('XSUPCONN', 'noConn', sub_name)
